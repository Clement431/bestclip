<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GenerateVideoController extends AbstractController
{
    /**
     * @Route("/generateVideo", name="generate_video")
     */
    public function index(): Response
    {
        return $this->render('generate_video/index.html.twig', [
            'controller_name' => 'GenerateVideoController',
        ]);
    }
}
