import { useCallback, useState } from "react";



function twitchHeader()
{
    var myHeaders = new Headers();
    myHeaders.append("client-id", "xeqrt92vmf6gzy83bnq6bhcojog0qm");
    myHeaders.append("Authorization", "Bearer 5ywax8chyfjcw5d7jopki1hivri11m");

    var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
    };
    return requestOptions;
}


export function usePaginatedFetch(url){

    const [loading, setloading] = useState(false)
    const [items, setItems] = useState([])
    const load = useCallback(async () => {
        setloading(true);
        const response = await fetch(url, twitchHeader() )
        const responseData = await response.json()
        if (response.ok) {
            setItems(responseData)

        }else{
            console.error(responseData);
        }
        setloading(false);
    }, [url])

    return {
        items,
        load,
        loading
    }
}
