<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\ConcateVideo;
use App\Service\DownloadVideo;
use App\Service\ApiTwitch;

class TestController extends AbstractController
{
   private ApiTwitch $apiTwitch;

    public function __construct(ApiTwitch $apiTwitch)
    {
    $this->apiTwitch = $apiTwitch;
    }

    /**
     * @Route("/test", name="test")
     */
    public function index(): Response
    {
        //$this->apiTwitch->getStreamer("zerator");
        //$this->apiTwitch->getClipByStreamer(114497555,'2021-07-07T00:00:00Z','2021-07-14T00:00:00Z');
        //$test = new ConcateVideo();
        //$test->concate();
       // $dlVideo = new DownloadVideo();
        //$dlVideo->download();
        return $this->render('test/index.html.twig', [
            'controller_name' => 'TestController',
        ]);
    }
}
