<?php
namespace App\Service;

class ConcateVideo
{

    public function concate(){

        $folderPath =   ("./tmpClip");
        $videoPath  = realpath("./video");
        $ffmpeg = \FFMpeg\FFMpeg::create([
            'ffmpeg.binaries'  => '..\bin\ffmpeg.exe',
            'ffprobe.binaries' => '..\bin\ffprobe.exe' 
       ]);
       
       $video = $ffmpeg->open($folderPath.'\video0.mp4');
       
       $video
           ->concat(array($folderPath.'\video0.mp4',$folderPath.'\video1.mp4' ,$folderPath.'\video2.mp4'))
           ->saveFromSameCodecs($videoPath.'\proute.mp4', TRUE);
    }

}