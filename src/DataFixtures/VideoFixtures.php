<?php

namespace App\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Video;

class VideoFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $video = new Video();
        $video->setName("video1");
        $video->setTime(new \DateTime("0:0:30"));
        $video->setUrl("./video/proute.mp4");
        $manager->persist($video);
        $manager->flush();
    }

}
