<?php

namespace App\DataFixtures;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;


class UserFixtures extends Fixture
{

    private $passwordHasher;

     public function __construct(UserPasswordHasherInterface $passwordHasher)
   {
         $this->passwordHasher = $passwordHasher;
     }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail("clembernard43@gmail.com");

        $user->setPassword($this->passwordHasher->hashPassword(
                    $user,
                    'bonjour'
                ));

        $manager->persist($user);

        $manager->flush();
    }
}
