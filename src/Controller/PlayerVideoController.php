<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\VideoRepository;
use Symfony\Component\Routing\Annotation\Route;

class PlayerVideoController extends AbstractController
{
    /**
     * @Route("/playerVideo/{videoId}", name="player_video")
     */
    public function index(int $videoId, VideoRepository $videoRepository): Response
    {

        $video = $videoRepository->findVideoAndClipsByIdVideo(11);
        dump($video);

        return $this->render('player_video/index.html.twig', [
            'video' => $video,
        ]);
    }

    /**
     * la reoute pour avoir une page d'acueil pas defaut avec un video du moi ou de la semaine
     * @Route("playerVideo", name="player_video_default")
     */
    public function FunctionName(): Response
    {
        return $this->render('player_video/index.html.twig', [
            'controller_name' => 'PlayerVideoController',
        ]);
    }
}
