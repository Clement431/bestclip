<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Clip;
use App\Entity\Video;

class ClipFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $video = new Video();
        $video->setName("videoWIthCLip");
        $video->setTime(new \DateTime("0:0:30"));
        $video->setUrl("./video/proute.mp4");

         $clip = new Clip();
         $clip->setTitle("clip1");
         $clip->setGame('minecraft');
         $clip->setBroadcasterName('aypierre');
         $clip->setBroadcasterUrl('https://www.twitch.tv/aypierre');
         $clip->setReplayUrl('https://www.twitch.tv/videos/1140960842');
         $clip->setTime(new \DateTime("0:0:30"));
         $clip->setVideo($video);

         $clip2 = new Clip();
         $clip2->setTitle("clip2");
         $clip2->setGame('valorant');
         $clip2->setBroadcasterName('jbzzed');
         $clip2->setBroadcasterUrl('https://www.twitch.tv/jbzzed');
         $clip2->setReplayUrl('https://www.twitch.tv/videos/1142537592');
         $clip2->setTime(new \DateTime("0:0:45"));
         $clip2->setVideo($video);


         $clip3 = new Clip();
         $clip3->setTitle("clip3");
         $clip3->setGame('WoW');
         $clip3->setBroadcasterName('zerator');
         $clip3->setBroadcasterUrl('https://www.twitch.tv/zerator');
         $clip3->setReplayUrl('https://www.twitch.tv/videos/1141878545');
         $clip3->setTime(new \DateTime("0:1:28"));
         $clip3->setVideo($video);

         $manager->persist($clip);
         $manager->persist($clip2);
         $manager->persist($clip3);

        $manager->flush();


    }
}
