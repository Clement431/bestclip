<?php
namespace App\Service;



use YoutubeDl\Options;
use YoutubeDl\YoutubeDl;
class DownloadVideo
{

    public function download(){

        $yt = new YoutubeDl();
        $yt->setBinPath('..\bin\youtube-dl.exe');
        $collection = $yt->download(
            Options::create()
                ->downloadPath('/tmpClip')
                ->url('https://clips.twitch.tv/SuccessfulHungrySandwichCoolCat-G1cAIzqLonJ56sxG',
                'https://clips.twitch.tv/GorgeousWanderingClintPhilosoraptor-R7v1taIgtTnX5f4y',
                'https://clips.twitch.tv/CarefulPolishedRuffFailFish-uq5BBuM_ZV7TqPsz'));
        
        foreach ($collection->getVideos() as $video) {
            if ($video->getError() !== null) {
                echo "Error downloading video: {$video->getError()}.";
            } else {
                echo $video->getTitle(); // Will return Phonebloks
                // $video->getFile(); // \SplFileInfo instance of downloaded file
            }

        }

    }

    public function downloadV2()
    {

    $yt = new YoutubeDl();
    $yt->onProgress(static function (string $progressTarget, string $percentage, string $size, string $speed, string $eta, ?string $totalTime): void 
    {
    echo "Download file: $progressTarget; Percentage: $percentage; Size: $size";
    if ($speed) {
        echo "; Speed: $speed";
    }
    if ($eta) {
        echo "; ETA: $eta";
    }
    if ($totalTime !== null) {
        echo "; Downloaded in: $totalTime";
    }
    });
    
         $yt->setBinPath('..\bin\youtube-dl.exe');
         $yt->download(
            Options::create()
                ->downloadPath('/tmpClip')
                ->url('https://clips.twitch.tv/SuccessfulHungrySandwichCoolCat-G1cAIzqLonJ56sxG')
        );
   
    }
}
