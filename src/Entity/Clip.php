<?php

namespace App\Entity;

use App\Repository\ClipRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClipRepository::class)
 */
class Clip
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $game;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $broadcasterName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $broadcasterUrl;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $replayUrl;

    /**
     * @ORM\Column(type="time")
     */
    private $time;

    /**
     * @ORM\ManyToOne(targetEntity=Video::class, inversedBy="clips",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $video;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getGame(): ?string
    {
        return $this->game;
    }

    public function setGame(string $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getBroadcasterName(): ?string
    {
        return $this->broadcasterName;
    }

    public function setBroadcasterName(string $broadcasterName): self
    {
        $this->broadcasterName = $broadcasterName;

        return $this;
    }

    public function getBroadcasterUrl(): ?string
    {
        return $this->broadcasterUrl;
    }

    public function setBroadcasterUrl(string $broadcasterUrl): self
    {
        $this->broadcasterUrl = $broadcasterUrl;

        return $this;
    }

    public function getReplayUrl(): ?string
    {
        return $this->replayUrl;
    }

    public function setReplayUrl(string $replayUrl): self
    {
        $this->replayUrl = $replayUrl;

        return $this;
    }

    public function getTime(): ?\DateTimeInterface
    {
        return $this->time;
    }

    public function setTime(\DateTimeInterface $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getVideo(): ?Video
    {
        return $this->video;
    }

    public function setVideo(?Video $video): self
    {
        $this->video = $video;

        return $this;
    }
}
