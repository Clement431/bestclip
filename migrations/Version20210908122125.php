<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210908122125 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE clip ADD video_id INT NOT NULL');
        $this->addSql('ALTER TABLE clip ADD CONSTRAINT FK_AD20146729C1004E FOREIGN KEY (video_id) REFERENCES video (id)');
        $this->addSql('CREATE INDEX IDX_AD20146729C1004E ON clip (video_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE clip DROP FOREIGN KEY FK_AD20146729C1004E');
        $this->addSql('DROP INDEX IDX_AD20146729C1004E ON clip');
        $this->addSql('ALTER TABLE clip DROP video_id');
    }
}
