import {render} from 'react-dom'
import { useState, useEffect } from "react";
import React from 'react'
import '../styles/select.css';


function twitchHeader()
{
    var myHeaders = new Headers();
    myHeaders.append("client-id", "xeqrt92vmf6gzy83bnq6bhcojog0qm");
    myHeaders.append("Authorization", "Bearer 5ywax8chyfjcw5d7jopki1hivri11m");

    var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
    };

    return requestOptions;
}

function StreamerSelect(){
    
    const [searchTerm, setSearchTerm] = useState("");
    const [searchResults, setSearchResults] = useState([]);
    const [data ,setData] = useState([]);

    const handleChange = event => {
        setSearchTerm(event.target.value);
      };

      useEffect(()=>{
          if (searchTerm) {
          const fetchData = async ()=> {
          const response = await fetch('https://api.twitch.tv/helix/search/channels?query='+searchTerm, twitchHeader() )
          const responseData = await response.json()
          setSearchResults(responseData)
                 };
              fetchData(); 
            }
            else{
              setSearchResults([])
            }
    },[searchTerm]);

    return (
        <div>
          <input
            type="text"
            placeholder="Search"
            value={searchTerm}
            onChange={handleChange}
          />
          <ul>
             {searchResults.data
             ? searchResults.data.map((item) => (
              <li key={item.id}> <img className ='icon-streamer' src={item.thumbnail_url}/> {item.display_name}</li>
            ))
            : ""}
          </ul>
        </div>
      );
}

class streamerSelect extends HTMLElement {

    connectedCallback(){
        render (<StreamerSelect></StreamerSelect>,this)
    }
}

customElements.define('post-streamer',streamerSelect)